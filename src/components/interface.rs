extern crate easycurses;

use std::collections::HashSet;
use std::iter::FromIterator;
use std::cmp::min;
use std::convert::TryFrom;

use easycurses::*;
use easycurses::Color::*;

use log::{ debug, error, warn };

use super::memory::MemoryMap;
use super::state::FrameStack;
use super::iff::Chunk;
use super::iff::quetzal::IFZS;
use super::instruction::{ Instruction, BranchOffset };
use super::InfocomError;

use std::path::Path;
use std::fs::File;
use std::fs;
use std::io::{ Write };

pub enum StatusLineFormat {
    SCORED,
    TIMED
}

pub trait Interface {
    fn output_char(&mut self, c: char);
    fn output_string(&mut self, s: &str);
    fn select_output_stream(&mut self, state: &mut FrameStack, stream: u8, table: Option<usize>);
    fn deselect_output_stream(&mut self, state: &mut FrameStack, stream: u8);
    fn print(&mut self, text: &str);
    fn new_line(&mut self);
    fn read(&mut self, state: &mut FrameStack, terminating_characters: HashSet<char>, max_chars: usize, timeout: Option<u16>) -> (String,bool);
    fn status_line(&mut self, name: &str, format: StatusLineFormat, v1: i16, v2: u16);
    fn split_window(&mut self, lines: u8);
    fn set_window(&mut self, window: Window);
    fn set_cursor(&mut self, r: u8, c: u8);
    fn get_cursor(&mut self) -> (u8, u8);
    fn save(&mut self, state: &mut FrameStack, instruction: &Instruction) -> Result<(),InfocomError>;
    fn restore(&mut self, state: &mut FrameStack) -> Result<BranchOffset,InfocomError>;
    fn beep(&mut self);
}

pub enum Window {
    Upper,
    Lower
}

pub struct Curses {
    pub window: EasyCurses,
    printed_lines: u32,
    selected_window: Window,
    split_height: i32,
    lower_window_cursor: (u8, u8),  // Relative cursor position within lower window buffer
    lower_window_buffer: Vec<Vec<char>>,
    upper_window_cursor: (u8, u8),
    upper_window_buffer: Vec<Vec<char>>,
    screen_size: (u8, u8),
    output_stream_selection: u8,
    output_stream_2: Option<File>,
    output_stream_3: Vec<u8>,
    output_stream_3_address: usize
}

impl Curses {
    pub fn new(mem: &mut MemoryMap) -> Curses {
        let mut window = EasyCurses::initialize_system().unwrap();
        window.auto_resize = false;
        window.resize(40,132);
        // V3 has a status line, so effective screen height is 1 row shorter
        if let Err(e) = mem.set_byte(0x20, 40) {
            error!("Error updating screen height in the header: {}", e.to_string());
        }
        if let Err(e) = mem.set_byte(0x21, 132) {
            error!("Error updating screen width in the header: {}", e.to_string());
        }
        window.set_scrolling(false);
        let f1 = mem.get_byte(0x01).unwrap();
        if let Err(e) = mem.set_byte(0x01, f1 | 0x20) {
            error!("Error updating flags1 in the header: {}", e.to_string());
        }
        window.set_echo(false);
        window.set_input_mode(easycurses::InputMode::Character);
        window.set_keypad_enabled(true);
        window.refresh();
        window.set_color_pair(colorpair!(White on Black));
        // Start with an empty screen
        let lower_window_buffer = vec![vec![' '; 132]; 39];
        let upper_window_buffer:Vec<Vec<char>> = Vec::new();

        Curses { window: window, printed_lines: 0, selected_window: Window::Lower, split_height: 0, 
                 screen_size: (39, 132), lower_window_cursor: (38, 0), lower_window_buffer, upper_window_cursor: (0, 0), upper_window_buffer,
                 output_stream_selection: 1,  output_stream_2: None, output_stream_3: Vec::new(), output_stream_3_address: 0 }
    }

    fn prompt(&mut self) {
        match self.selected_window {
            Window::Lower => {
                let rows = self.screen_size.0 - self.split_height as u8 - 1;
                let r = self.buffer_get_cursor().0;
        
                if self.printed_lines as u8 >= rows {
                    self.buffer_set_cursor(rows, 0);
                    self.buffer_print("[MORE]");
                    self.refresh();
                    self.window.get_input();
                    self.buffer_set_cursor(r, 0);
                    self.buffer_print("      ");
                    self.buffer_set_cursor(r, 0);
                    self.refresh();
                    self.printed_lines = 0;
                }
        
            },
            _ => {}
        }
    }

    fn buffer_get_cursor(&self) -> (u8, u8) {
        match self.selected_window {
            Window::Upper => self.upper_window_cursor,
            Window::Lower => self.lower_window_cursor
        }
    }

    fn buffer_set_cursor(&mut self, r: u8, c: u8) {
        match self.selected_window {
            Window::Upper => self.upper_window_cursor = (r, c),
            Window::Lower => self.lower_window_cursor = (r, c)
        }
    }

    fn buffer_print_char(&mut self, ch: char) {
        let (r, c) = self.buffer_get_cursor();
        match self.selected_window {
            Window::Upper => self.upper_window_buffer[r as usize][c as usize] = ch,
            Window::Lower => self.lower_window_buffer[r as usize][c as usize] = ch
        }
        self.advance_cursor();
    }

    fn buffer_print(&mut self, s: &str) {
        for c in s.chars() {
            self.buffer_print_char(c);
        }
    }

    fn refresh(&mut self) {
        match self.selected_window {
            Window::Upper => {
                for i in 0 .. self.split_height {
                    self.window.move_rc(i + 1, 0);
                    for j in 0 .. self.screen_size.1 {
                        self.window.print_char(self.upper_window_buffer[i as usize][j as usize]);
                    }
                }
                self.window.move_rc(self.upper_window_cursor.0 as i32 + 1, self.upper_window_cursor.1 as i32);
            },
            Window::Lower => {
                for i in 0 .. self.screen_size.0 - self.split_height as u8 {
                    self.window.move_rc(self.split_height + i as i32 + 1, 0);
                    for j in 0 .. self.screen_size.1 {
                        self.window.print_char(self.lower_window_buffer[i as usize][j as usize]);
                    }
                }
                self.window.move_rc(self.lower_window_cursor.0 as i32 + self.split_height + 1, self.lower_window_cursor.1 as i32);
            }
        }

        self.window.refresh();
    }

    fn advance_cursor(&mut self) {
        let (r, c) = self.buffer_get_cursor();
        match self.selected_window {
            Window::Upper => {
                // Advance the cursor to the end of the line
                if c < self.screen_size.1 - 1 {
                    self.buffer_set_cursor(r, c + 1);
                } else {
                    // Wrap to the next line until the end of the window
                    if r < self.split_height as u8 - 1 {
                        self.buffer_set_cursor(r + 1, 0);
                    }
                }
            },
            Window::Lower => {
                // Advance cursor to the end of the line
                if c < self.screen_size.1 - 1 {
                    self.buffer_set_cursor(r, c + 1)
                } else if r < self.screen_size.0 - self.split_height as u8 - 1 {
                    self.buffer_set_cursor(r + 1, 0);
                } else {
                    self.scroll(1);
                    self.buffer_set_cursor(self.screen_size.0 - self.split_height as u8 - 1, 0);
                }
            }
        }
    }

    fn scroll(&mut self, rows: i8) {
        if rows > 0 {
            // scroll lower window up by rows
            for _ in 0 .. rows {
                // Remove top row
                self.lower_window_buffer.remove(0);
                // Add a blank row to the bottom of the screen
                self.lower_window_buffer.push(vec![' '; self.screen_size.1 as usize]);
            }
        } else {
            // scroll lower window down by rows
            for _ in 0 .. rows {
                self.lower_window_buffer.pop();
                self.lower_window_buffer.insert(0, vec![' '; self.screen_size.1 as usize]);
            }
        }

        self.refresh();
    }
}

impl Interface for Curses {    
    fn select_output_stream(&mut self, state: &mut FrameStack, stream: u8, table: Option<usize>) {
        let bit = 1 << (stream - 1);

        if stream == 2 {
            debug!("Selecting output stream 2: {:?}", self.output_stream_2);
            match self.output_stream_2 {
                None => {
                    self.output_string("Enter filename: ");
                    let mut filename = self.read(state, HashSet::from_iter("\n".chars()), 64, None).0;
                    // Strip the terminator
                    filename.pop();
            
                    let path = Path::new(&filename);
                    match File::create(path) {
                        Err(e) => warn!("Error creating save file {}: {}", filename, e.to_string()),
                        Ok(f) => self.output_stream_2 = Some(f)
                    };
                },
                _ => {}
            }
        } else  if stream == 3 {
            self.output_stream_3 = Vec::new();
            self.output_stream_3_address = table.unwrap();
        }

        self.output_stream_selection |= bit;
    }

    fn deselect_output_stream(&mut self, state: &mut FrameStack, stream: u8) {
        let bit = 1 << (stream - 1);
        self.output_stream_selection &= bit ^ 0xFF;
        if stream == 3 {
            state.set_word(self.output_stream_3_address, self.output_stream_3.len() as u16);
            for (i, b) in self.output_stream_3.iter().enumerate() {
                state.set_byte(self.output_stream_3_address + 2 + i, *b);
            }
        }
    }
    
    fn output_char(&mut self, ch: char) {
        // When stream 3 is selected, no output is sent to any other stream
        if self.output_stream_selection & 0x4 == 4 {
            self.output_stream_3.append(&mut vec![ch as u8]);
        } else {
            if self.output_stream_selection & 0x1 == 1 {
                self.buffer_print_char(ch);
            }

            if self.output_stream_selection & 0x2 == 2 {
                self.output_stream_2.as_ref().unwrap().write(&vec![ch as u8]);
            }
        }
    }

    fn output_string(&mut self, s: &str) {
        // When stream 3 is selected, no output is sent to any other stream
        if self.output_stream_selection & 0x4 == 4 {
            self.output_stream_3.append(&mut s.as_bytes().to_vec());
        } else {
            if self.output_stream_selection & 0x1 == 1 {
                self.buffer_print(s);
            }

            if self.output_stream_selection & 0x2 == 2 {
                self.output_stream_2.as_ref().unwrap().write_all(s.as_bytes());
            }
        }
    }

    fn print(&mut self, text: &str) {
        let words: Vec<&str> = text.split(' ').collect();
        let (_, cols) = self.screen_size;
        for (i, word) in words.iter().enumerate() {
            let (_, c) = self.buffer_get_cursor();
            if i > 0 && c < cols as u8 - 1 {
                self.output_char(' ');
                // self.buffer_print_char(' ');
            }

            // Check if the word contains any newline characters
            let mut slice = *word;
            while let Some(j) = slice.find('\n') {
                let (_,c) = self.buffer_get_cursor();
                let part = &slice[0..j];
                // Check if the slice is too long to fit in the remaining space
                if part.len() + 1 >= cols as usize - c as usize {
                    self.new_line();
                }

                // Print the part before the new line
                self.output_string(part);
                // self.buffer_print(part);

                // ... and the new line
                self.new_line();

                // Check if there's more after the newline
                if j < slice.len() {
                    slice = &slice[j+1..];
                } else {
                    slice = "";
                }
            }
            
            // If there's any text left over, print it, too
            if slice.len() > 0 {
                let (_,c) = self.buffer_get_cursor();
                if slice.len() >= cols as usize - c as usize {
                    self.new_line();
                }

                self.output_string(slice);
                // self.buffer_print(slice);
            }
        }

        self.refresh();
    }

    fn new_line(&mut self) {
        if self.output_stream_selection & 0x2 == 2 {
            self.output_stream_2.as_ref().unwrap().write(&vec![0x0D]);
        }

        let rows = self.screen_size.0 - self.split_height as u8;
        let (r, _) = self.buffer_get_cursor();

        if r == rows - 1 {
            self.scroll(1);
            self.buffer_set_cursor(r, 0);
        } else {
            self.buffer_set_cursor(r + 1, 0);
        }
        self.printed_lines += 1;
        self.refresh();
        self.prompt();
    }

    fn read(&mut self, state: &mut FrameStack, terminating_characters: HashSet<char>, max_chars: usize, timeout: Option<u16>) -> (String,bool) {
        self.printed_lines = 0;
        let mut result = String::new();
        match timeout {
            Some(s) => {
                let t:i32 = (s as i32 * 1000) / 10;
                self.window.set_input_timeout(TimeoutMode::WaitUpTo(t));
            },
            None => {
                self.window.set_input_timeout(TimeoutMode::Never);
            }
        }
        loop {
            self.refresh();
            if let Some(e) = self.window.get_input() {
                match e {
                    easycurses::Input::KeyEnter => {
                        result.push('\n');
                        break;
                    },
                    easycurses::Input::KeyResize => {
                        debug!("resizing is not very well supported.");
                        // TODO: Try to preserve the offscreen buffer, either clipping it or adding empty space.
                        // More sophisticated resize handling is left to platform-specific interface implementations.
                        self.window.resize(0,0);
                        self.window.refresh();
                        let (r,c) = self.window.get_row_col_count();
                        self.screen_size = (r as u8 - 1, c as u8);
                        // For now, clear the screen. It's gross, but a little fiddling with seastalker shows that
                        // the sonar window will redraw after the next command and other than loss the text on the screen,
                        // the show will go on.
                        let lwb = vec![vec![' '; c as usize]; r as usize - 1 - self.split_height as usize];
                        self.lower_window_buffer = lwb;
                        // If the cursor is no longer on the screen, move it to the bottom row and/or right-most column.
                        let (lr, lc) = self.lower_window_cursor;
                        self.lower_window_cursor = (min(lr, r as u8 - 1 - self.split_height as u8), min(lc, c as u8 - 1));
                        let uwb = vec![vec![' '; c as usize]; self.split_height as usize];
                        self.upper_window_buffer = uwb;
                        let (ur, uc) = self.upper_window_cursor;
                        self.upper_window_cursor = (min(ur, self.split_height as u8), min(uc, c as u8 - 1));
                        self.printed_lines = 0;
                        self.refresh();

                        if let Err(e) = state.set_byte(0x20, r as u8) {
                            error!("Error updating screen height in the header: {}", e.to_string());
                        }
                        if let Err(e) = state.set_byte(0x21, c as u8) {
                            error!("Error updating screen width in the header: {}", e.to_string());
                        } 
                    },
                    easycurses::Input::Character(c) => {
                        if terminating_characters.contains(&c) {
                            result.push(c);
                            self.new_line();
                            break;
                        }

                        if c as u16 == 8 {
                            if result.len() > 0 {
                                result.pop();
                                let (r,c) = self.buffer_get_cursor();
                                self.lower_window_buffer[r as usize][c as usize - 1] = ' ';
                                self.buffer_set_cursor(r, c - 1);
                            }
                        // TODO: Filter the specific accented characters that we support
                        // TODO: include A2 punctuation
                        } else if ((c as u16) > 31 && (c as u16) < 127) || ((c as u16) > 155 && (c as u16) < 256) {
                            if result.len() < max_chars {
                                self.output_char(c);
                                // self.buffer_print_char(c);
                                result.push(c);
                            }
                        }
                    },
                    _ => {}
                }
            } else {
                // Need to return the partial input here and flag it as interrupted
                return (result,true);
            }
        }
        
        self.refresh();
        (result,false)
    }

    fn status_line(&mut self, name: &str, format: StatusLineFormat, v1: i16, v2: u16) {
        let (r,c) = self.window.get_cursor_rc();
        let width = self.window.get_row_col_count().1;

        self.window.move_rc(0, 0);
        self.window.set_color_pair(colorpair!(Black on White));
        
        self.window.print_char(' ');
        self.window.print(name);

        let left_str = match format {
            StatusLineFormat::SCORED => {
                format!("Score: {:3}    Turn: {:4} ", v1, v2)
            },
            StatusLineFormat::TIMED => {
                let hour = v1.rem_euclid(12);
                let am_pm = if v1 > 11 { "PM" } else { "AM" };
                format!("{:2}:{:02} {} ", hour, v2, am_pm)
            }
        };

        let padding = width as usize - 1 - name.len() - left_str.len();
        for _ in 0..padding {
            self.window.print_char(' ');
        }
        self.window.print(left_str);

        self.window.set_color_pair(colorpair!(White on Black));
        self.window.move_rc(r, c);
        self.window.refresh();
    }

    fn get_cursor(&mut self) -> (u8,u8) {
        match self.selected_window {
            Window::Upper => (self.upper_window_cursor.0 + 1, self.upper_window_cursor.1 + 1),
            Window::Lower => (self.lower_window_cursor.0 + self.split_height as u8 + 1, self.lower_window_cursor.1 + 1)
        }
    }

    fn set_cursor(&mut self, r: u8, c: u8) {
        match self.selected_window {
            Window::Upper => {
                if r - 1 >= self.split_height as u8 {
                    self.upper_window_cursor = (self.split_height as u8 - 1, 0);
                } else {
                    self.upper_window_cursor = (r - 1, c - 1);
                }
            },
            Window::Lower => {
                if r - 1 >= self.screen_size.0 - self.split_height as u8 - 1 {
                    self.lower_window_cursor = (self.screen_size.0 - self.split_height as u8 - 1, 0);
                } else {
                    self.lower_window_cursor = (r - self.split_height as u8 - 1, c);
                }
            }
        }
    }

    fn set_window(&mut self, window: Window) {
        match window {
            Window::Upper => self.upper_window_cursor = (0, 0),
            _ => {}
        }

        self.selected_window = window;
    }

    fn split_window(&mut self, lines: u8) {
        let old_split = self.split_height as u8;
        self.split_height = lines as i32;

        // Upper window size is increasing
        if lines > old_split as u8 {
            // Clear the upper window and position the cursor 
            self.upper_window_cursor = (0,0);
            self.upper_window_buffer = vec![vec![' '; self.screen_size.1 as usize]; lines as usize];
            // Remove the top of the lower window buffer that is being overwritten by the upper window
            for _ in 0 .. lines - old_split {
                self.lower_window_buffer.remove(0);
            }

            // Reposition the lower window cursor if it is now off screen
            if self.lower_window_cursor.0 > self.screen_size.0 - lines - 1 {
                self.lower_window_cursor = (self.screen_size.0 - lines - 1, self.lower_window_cursor.1);
            }
        } else {
            // Copy the upper window lines that are no longer in the upper window to the top of the lower window
            for _ in 0 .. old_split - lines {
                self.lower_window_buffer.insert(0, self.upper_window_buffer.pop().unwrap());
            }
            // Adjust relative cursor position
            self.lower_window_cursor = (self.lower_window_cursor.0 + old_split - lines, self.lower_window_cursor.1);
        }

        self.printed_lines = 0;
    }

    fn save(&mut self, state: &mut FrameStack, instruction: &Instruction) -> Result<(),InfocomError> {
        self.output_string("Enter filename: ");
        let mut filename = self.read(state, HashSet::from_iter("\n".chars()), 64, None).0;
        // Strip the terminator
        filename.pop();

        let path = Path::new(&filename);
        let mut file = match File::create(path) {
            Err(e) => return Err(InfocomError::Memory(format!("Error creating save file {}: {}", filename, e.to_string()))),
            Ok(f) => f
        };

        let ifzs = IFZS::new(state, instruction);
        let l = ifzs.length();

        match file.write_all(b"FORM") {
            Ok(_) => {},
            Err(e) => return Err(InfocomError::Memory(format!("Error writing file {}: {}", filename, e.to_string())))
        }

        match file.write_all(&vec![((l >> 24) as u8 & 0xFF),
                             ((l >> 16) as u8 & 0xFF),
                             ((l >> 8) as u8 & 0xFF),
                             l as u8 & 0xFF]) {
            Ok(_) => {}
            Err(e) => return Err(InfocomError::Memory(format!("Error writing file {}: {}", filename, e.to_string())))
        }

        match file.write_all(&ifzs.as_bytes()) {
            Ok(_) => Ok(()),
            Err(e) => Err(InfocomError::Memory(format!("Error writing file {}: {}", filename, e.to_string())))
        }
    }

    fn restore(&mut self, state: &mut FrameStack) -> Result<BranchOffset,InfocomError> {
        self.output_string("Enter filename: ");
        let mut filename = self.read(state, HashSet::from_iter("\n".chars()), 64, None).0;
        // Strip the terminator
        filename.pop();

        let path = Path::new(&filename);

        match fs::read(path) {
            Ok(iff) => {
                let ifzs = IFZS::try_from(&iff)?;
                debug!("Verify IFhd: {:?}", ifzs.ifhd);
                if ifzs.ifhd.verify(state.get_memory()) {
                    if let Some(u) = ifzs.umem {
                        debug!("Restore UMem");
                        state.restore_memory(&u.data())?;
                    } else if let Some(c) = ifzs.cmem {
                        debug!("Restore CMem");
                        state.restore_memory(&c.decompress(state.get_memory()))?;
                    }
                    debug!("Restore Stks");
                    state.restore_frames(&mut ifzs.stks.as_frames(), ifzs.ifhd.pc as usize)
                } else {
                    Err(InfocomError::Memory(format!("Save file doesn't match the executing story file")))
                }
            }
            Err(e) => Err(InfocomError::Memory(format!("Error reading file {}: {}", filename, e)))
        }
    }

    fn beep(&mut self) {
        // TODO: For windows, at least, this plays the system "alert" sound, which isn't necessarily a beep
        // Add both short and long beep sounds somewhere and use those instead.
        self.window.beep();
    }
}