use super::super::InfocomError;
use super::{ Chunk, u32_to_bytes, bytes_to_u32, bytes_to_string };

use log::{ info };
use std::convert::TryFrom;

pub struct RIdxIndex {
    usage: String,
    number: u32,
    start: u32
}

impl From<&Vec<u8>> for RIdxIndex {
    fn from(data: &Vec<u8>) -> RIdxIndex {
        let usage = bytes_to_string(data, 0, 4);
        let number = bytes_to_u32(data, 4);
        let start = bytes_to_u32(data, 8);

        RIdxIndex { usage, number, start }
    }
}

impl RIdxIndex {
    pub fn as_bytes(&self) -> Vec<u8> {
        let mut r = Vec::new();
        r.append(&mut self.usage.as_bytes().to_vec());
        r.append(&mut u32_to_bytes(self.number));
        r.append(&mut u32_to_bytes(self.start));

        r
    }
}

pub struct RIdx {
    num: u32,
    index_entries: Vec<RIdxIndex>
}

impl Chunk for RIdx {
    fn id(&self) -> String {
        String::from("RIdx")
    }

    fn length(&self) -> u32 {
        let l:u32 = 4 + 12 * self.index_entries.len() as u32;
        l
    }

    fn data(&self) -> Vec<u8> {
        let mut r = Vec::new();
        r.append(&mut u32_to_bytes(self.num));
        for i in &self.index_entries {
            r.append(&mut i.as_bytes());
        }

        r
    }
}

impl From<&Vec<u8>> for RIdx {
    fn from(data: &Vec<u8>) -> RIdx {
        let num = bytes_to_u32(data, 0);
        let mut index_entries = Vec::new();
        for i in 0..num {
            let offset = 4 + (i as usize * 12);
            let index = RIdxIndex::from(&data[offset..offset+12].to_vec());
            index_entries.push(index);
        }

        RIdx { num, index_entries }
    }
}

pub struct ZCOD {
    data: Vec::<u8>
}

impl Chunk for ZCOD {
    fn id(&self) -> String {
        String::from("ZCOD")
    }

    fn length(&self) -> u32 {
        self.data.len() as u32
    }

    fn data(&self) -> Vec<u8> {
        self.data.clone()
    }
}

impl From<&Vec<u8>> for ZCOD {
    fn from(data: &Vec<u8>) -> ZCOD {
        ZCOD { data: data.clone() }
    }
}

pub struct IFRS {
    ridx: RIdx,
    exec: ZCOD,    
}

impl Chunk for IFRS {
    fn id(&self) -> String {
        String::from("IFRS")
    }

    fn length(&self) -> u32 {
        let mut l:u32 = 4;
        l += 8 + self.ridx.length();
        l += 8 + self.exec.length();

        l
    }

    fn data(&self) -> Vec<u8> {
        let mut r = Vec::new();

        r.append(&mut b"IFRS".to_vec());
        r.append(&mut self.ridx.as_bytes());
        r.append(&mut self.exec.as_bytes());

        r
    }

    fn as_bytes(&self) -> Vec<u8> {
        self.data()
    }
}

impl TryFrom<&Vec<u8>> for IFRS {
    type Error = InfocomError;

    fn try_from(data: &Vec<u8>) -> Result<IFRS,Self::Error> {
        let form = bytes_to_string(data, 0, 4);
        if form.as_str() != "FORM" {
            return Err(InfocomError::Memory(format!("Invalid IFF form: '{}'", form)));
        }

        let length = bytes_to_u32(data, 4) as usize;

        let sub_id = bytes_to_string(data, 8, 4);
        if sub_id.as_str() != "IFRS" {
            return Err(InfocomError::Memory(format!("Invalid FORM sub-ID: '{}'", sub_id)));
        }

        let mut offset:usize = 12;
        let limit = length + 8;

        let mut ridx = None;
        let mut exec = None;

        while offset < limit {
            let id = bytes_to_string(data, offset, 4);
            let len = bytes_to_u32(data, offset + 4) as usize;
            match id.as_str() {
                "RIdx" => ridx = Some(RIdx::from(&data[offset + 8..offset + 8 + len].to_vec())),
                "ZCOD" => exec = Some(ZCOD::from(&data[offset + 8..offset + 8 + len].to_vec())),
                _ => info!("Ignoring chunk type '{}'", id)
            }

            offset += 8 + len;
        }

        Ok(IFRS { ridx: ridx.unwrap(), exec: exec.unwrap() })
    }
}
