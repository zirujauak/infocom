use super::super::InfocomError;
use super::{ Chunk, u16_to_bytes, bytes_to_u16, bytes_to_u32, bytes_to_string };
use super::super::memory::MemoryMap;
use super::super::instruction::{ BranchOffset, Instruction };
use super::super::state::{ Frame, FrameStack, Routine };

use log::{ debug, info };
use std::convert::TryFrom;

// IFhd check - data about the story file that wil be used to verify that a save file matches
// the story currently executing.
#[derive(Debug)]
pub struct IFhd {
    pub release: u16,
    pub serial_number: Vec<u8>,
    pub checksum: u16,
    pub pc: u32,
}

impl Chunk for IFhd {
    fn id(&self) -> String {
        String::from("IFhd")
    }

    fn length(&self) -> u32 {
        13
    }

    fn data(&self) -> Vec<u8> {
        let mut data:Vec<u8> = Vec::new();
        data.append(&mut u16_to_bytes(self.release));
        for i in 0..self.serial_number.len() {
            data.push(self.serial_number[i]);
        }
        data.append(&mut u16_to_bytes(self.checksum));
        data.push((self.pc >> 16) as u8 & 0xFF);
        data.push((self.pc >> 8) as u8 & 0xFF);
        data.push(self.pc as u8 & 0xFF);

        data
    }
}

impl From<&Vec<u8>> for IFhd {
    fn from(data: &Vec<u8>) -> IFhd {
        let release = bytes_to_u16(&data, 0);
        let serial_number = data[2..8].to_vec();
        let checksum = bytes_to_u16(&data, 8);

        // This is the only 3-byte construct, no magic conversion function
        let pc = (((data[10] as u32) << 16) & 0xFF0000) |
                 (((data[11] as u32) << 8) & 0xFF00) |
                 ((data[12] as u32) & 0xFF);
        
        IFhd { release, serial_number, checksum, pc }
    }
}

impl IFhd {
    pub fn new_v1(mem: &MemoryMap, branch_offset: &BranchOffset) -> IFhd {
        let release = mem.get_word(0x02).unwrap();
        let mut serial_number = Vec::new();
        for i in 18..24 {
            serial_number.push(mem.get_byte(i).unwrap());
        }

        let mut checksum = mem.get_word(0x1c).unwrap();
        if checksum == 0 {
            checksum = mem.checksum();
        }
        
        let pc = branch_offset.location as u32;

        IFhd { release, serial_number, checksum, pc }
    }

    pub fn verify(&self, mem: &MemoryMap) -> bool {
        let release = mem.get_word(0x02).unwrap();
        if release != self.release {
            info!("IFhd release does not match: ${:04x} != ${:04x}", release, self.release);
            return false;
        }

        let mut serial_number = Vec::new();
        for i in 18..24 {
            serial_number.push(mem.get_byte(i).unwrap());
        }

        if serial_number != self.serial_number {
            info!("IFhd serial number does not match: {:?} != {:?}", serial_number, self.serial_number);
            return false;
        }

        let mut checksum = mem.get_word(0x1C).unwrap();
        if checksum == 0 {
            checksum = mem.checksum();
        }

        if checksum != self.checksum {
            info!("IFhd checksum does not match: ${:04x} != ${:04x}", checksum, self.checksum);
            return false;
        }

        true
    }
}

// UMem chunk - uncompressed copy of dynamic memory at save
pub struct UMem {
    pub data: Vec<u8>
}

impl Chunk for UMem {
    fn id(&self) -> String {
        String::from("UMem")
    }

    fn length(&self) -> u32 {
        self.data.len() as u32
    }

    fn data(&self) -> Vec<u8> {
        self.data.clone()
    }
}

impl From<&Vec<u8>> for UMem {
    fn from(data: &Vec<u8>) -> UMem {
        UMem { data: data.clone() }
    }
}

// CMem chunk - compressed dynamic memory at save.
pub struct CMem {
    data: Vec<u8>
}

impl Chunk for CMem {
    fn id(&self) -> String {
        String::from("CMem")
    }

    fn length(&self) -> u32 {
        self.data.len() as u32
    }

    fn data(&self) -> Vec<u8> {
        self.data.clone()
    }
}

impl CMem {
    // Compresses the current state of dynamic memory per Quetzal standard S3.2
    pub fn new(mem: &MemoryMap) -> CMem {
        CMem { data: CMem::compress(mem) }
    }

    fn compress(mem: &MemoryMap) -> Vec<u8> {
        let mut xor_data = Vec::new();

        // XOR current state with original state
        let orig = mem.get_dynamic_restore();
        let cur = mem.get_memory();
        for i in 0..orig.len() {
            xor_data.push(orig[i] ^ cur[i]);
        }

        let mut run_length = 0;
        let mut cmp_data = Vec::new();
        for i in 0..xor_data.len() {
            // RLE up to 256 consecutive 0 bytes.
            if xor_data[i] == 0 {
                // If this is the 256th consecutive 0, dump 0,255 to the compressed data
                if run_length == 255 {
                    cmp_data.push(0);
                    cmp_data.push(0xFF);
                    run_length = 0;
                } else {
                    run_length += 1;
                }
            } else {
                // Output any pending RL of 0 bytes
                if run_length > 0 {
                    cmp_data.push(0);
                    cmp_data.push(run_length - 1);
                    run_length = 0;
                }
                cmp_data.push(xor_data[i]);
            }
        }

        // If the next-to-last byte is a '0', then trip the zero run from the end.
        while cmp_data[cmp_data.len() - 2] == 0 {
            cmp_data.pop();
            cmp_data.pop();
        }
        cmp_data
    }

    pub fn decompress(&self, mem: &MemoryMap) -> Vec<u8> {
        let mut xor_data = Vec::new();
        let mut iter = self.data.iter();

        // Uncompress the data
        while let Some(i) = iter.next() {
            // Compressed 0 run of up to 256 bytes 
            if *i == 0 {
                let c = iter.next().unwrap();
                xor_data.append(&mut vec![0; *c as usize + 1]);
                // for _ in 0..*c+1 {
                //     xor_data.push(0);
                // }
            } else {
                xor_data.push(*i);
            }
        }

        let d = mem.get_dynamic_restore();
        let mut r = Vec::new();
        for i in 0..d.len() {
            // Assume 0 if the compressed data is shorter than dynamic memory
            let xor = {
                if xor_data.len() > i {
                    xor_data[i]
                } else {
                    0
                }
            };
            
            r.push(xor ^ d[i]);
        }
        
        r
    }
}

// `data` is assumed to be compressed (such as when loading from a save file).  Use CMem::new() to
// create a new struct from uncompressed memory.
impl From<&Vec<u8>> for CMem {
    fn from(data: &Vec<u8>) -> CMem {
        CMem { data: data.clone() }
    }
}

// Frame element of the Stks chunk
struct StkFrame {
    return_pc: u32,
    flags: u8,
    store_variable: u8,
    arguments: u8,
    stack_words: u16,
    local_variables: Vec<u16>,
    stack: Vec<u16>,
}

impl From<&Vec<u8>> for StkFrame {
    fn from(data: &Vec<u8>) -> StkFrame {
        let return_pc = (((data[0] as u32) << 16) & 0xFF0000) |
                        (((data[1] as u32) << 8) & 0xFF00) |
                        ((data[2] as u32) & 0xFF);
        let flags = data[3];
        let var_count = flags & 0xF;
        let store_variable = data[4];
        let arguments = data[5];
        let stack_words = bytes_to_u16(data, 6);
        let mut local_variables = Vec::new();
        for i in 0..var_count {
            local_variables.push(bytes_to_u16(data, 8 + (i as usize * 2)));
        }

        let mut stack = Vec::new();
        for i in 0..stack_words {
            stack.push(bytes_to_u16(data, 8 + (var_count as usize * 2) + (i as usize * 2)));
        }

        StkFrame { return_pc, flags, store_variable, arguments, stack_words, local_variables, stack }
    }
}

impl StkFrame {
    pub fn dummy_frame(stack: &Vec<u16>) -> StkFrame {
        let stack_words = stack.len() as u16;
        let mut stk = Vec::new();
        for i in 0 .. stack.len() {
            stk.push(stack[i]);
        }

        StkFrame { return_pc: 0, flags: 0, store_variable: 0, arguments: 0, stack_words, local_variables: Vec::new(), stack: stk }
    }

    pub fn len(&self) -> u32 {
        // Return PC = 3 bytes
        // flags = 1 byte
        // store variable = 1 byte
        // arguments = 1 byte
        // stack size = 2 bytes
        let mut l:u32 = 8;
        let var_count = self.flags & 0xF;
        l += var_count as u32 * 2;
        l += self.stack_words as u32 * 2;
        l
    }

    pub fn as_frame(&self) -> Frame {
        let local_variables = self.local_variables.clone();
        let mut argument_count = 0;
        for i in 0 .. 7 {
            if self.arguments >> i & 1 == 1 {
                argument_count += 1;
            }
        }
        let stack = self.stack.clone();
        let pc = 0; // This should get filled in when the following frame returns
        let return_variable = if self.flags & 0x10 == 0x10 {
            Some(self.store_variable)
        } else {
            None
        };

        let return_address = self.return_pc as usize;
        // Dummy routine to make the struct happy
        let routine = Routine { address: 0, default_variables: Vec::new(), instruction_address: 0 };
        Frame  { routine, local_variables, argument_count, stack, pc, return_variable, return_address, interrupt_routine: false }
    }

    pub fn as_bytes(&self) -> Vec<u8> {
        let mut b = Vec::new();

        // return pc
        b.push(((self.return_pc >> 16) & 0xFF) as u8);
        b.push(((self.return_pc >> 8) & 0xFF) as u8);
        b.push((self.return_pc & 0xFF) as u8);

        // flags
        b.push(self.flags);

        // store variable
        b.push(self.store_variable);

        // arguments
        b.push(self.arguments);

        // stack usage
        b.push(((self.stack_words >> 8) & 0xFF) as u8);
        b.push((self.stack_words & 0xFF) as u8);

        // local variables
        for v in self.local_variables.iter() {
            b.push(((v >> 8) & 0xFF) as u8);
            b.push((v & 0xFF) as u8);
        }

        // stack
        for v in self.stack.iter() {
            b.push(((v >> 8) & 0xFF) as u8);
            b.push((v & 0xFF) as u8);
        }

        b
    }
}

impl From<&Frame> for StkFrame {
    fn from(frame: &Frame) -> StkFrame {
        let return_pc = frame.return_address as u32;
        let mut flags = if let None = frame.return_variable {
            0x10
        } else {
            0
        };
        flags |= frame.local_variables.len() as u8;
        let store_variable = match frame.return_variable {
            Some(v) => v,
            None => 0
        };

        let arguments = if frame.argument_count > 0 {
            let mut a = 1;
            for _ in 1..frame.argument_count {
                a = (a << 1) | 1;
            }
            a
        } else {
            0
        };
        let stack_words = frame.stack.len() as u16;
        let local_variables = frame.local_variables.clone();

        // Read frame stack from bottom up.
        let mut stack = Vec::new();
        for i in 0..frame.stack.len() {
            stack.push(frame.stack[i]);
        }

        StkFrame{ return_pc, flags, store_variable, arguments, stack_words, local_variables, stack }
    }
}

// Stks chunk
pub struct Stks {
    frames: Vec<StkFrame>
}

impl Chunk for Stks {
    fn id(&self) -> String {
        String::from("Stks")
    }

    fn length(&self) -> u32 {
        let mut l = 0;
        for f in &self.frames {
            l += f.len();
        }

        l
    }

    fn data(&self) -> Vec<u8> {
        let mut r = Vec::new();
        for f in &self.frames {
            r.append(&mut f.as_bytes());
        }

        r
    }
}

impl Stks {
    pub fn as_frames(&self) -> Vec<Frame> {
        let mut f = Vec::new();
        for s in self.frames.iter() {
            f.push(s.as_frame());
        }

        f
    }
}

impl From<&FrameStack<'_>> for Stks {
    fn from(frame_stack: &FrameStack<'_>) -> Stks {
        let mut frames = Vec::new();
        // The first frame is the dummy
        let f = &frame_stack.stack[0];
        frames.push(StkFrame::dummy_frame(&f.stack));
        for i in 1..frame_stack.stack.len() {
            frames.push(StkFrame::from(&frame_stack.stack[i]));
        }

        Stks { frames }
    }
}

impl From<&Vec<u8>> for Stks {
    fn from(data: &Vec<u8>) -> Stks {
        let mut offset = 0;
        let mut frames = Vec::new();
        while offset < data.len() {
            let s = StkFrame::from(&data[offset..].to_vec());
            offset += s.len() as usize;
            frames.push(s);
        }

        Stks { frames }
    }
}

// IFZS
pub struct IFZS {
    pub ifhd: IFhd,
    pub cmem: Option<CMem>,
    pub umem: Option<UMem>,
    pub stks: Stks,
}

impl Chunk for IFZS {
    fn id(&self) -> String {
        String::from("IFZS")
    }

    fn length(&self) -> u32 {
        // Start with sub-ID
        let mut l:u32 = 4;
        // IFhd ID (4 bytes) + length (4 bytes) + data
        l += 8 + self.ifhd.length();
        if l.rem_euclid(2) == 1 {
            l += 1;
        }

        // UMem/CMem ID + length + data
        if let Some(u) = &self.umem {
            l += 8 + u.length();
        } else if let Some(c) = &self.cmem {
            l += 8 + c.length();
        }
        if l.rem_euclid(2) == 1 {
            l += 1;
        }

        // Stks ID + length + data
        l += 8 + self.stks.length();
        if l.rem_euclid(2) == 1 {
            l += 1;
        }

        l
    }

    fn data(&self) -> Vec<u8> {
        let mut r:Vec<u8> = Vec::new();

        // Sub-ID
        r.append(&mut "IFZS".as_bytes().to_vec());
        // IFhd chunk
        r.append(&mut self.ifhd.as_bytes());
        // UMem/CMem chunk
        if let Some(u) = &self.umem {
            r.append(&mut u.as_bytes());
        } else if let Some(c) = &self.cmem {
            r.append(&mut c.as_bytes());
        } // else error
        // Stks chunk
        r.append(&mut self.stks.as_bytes());

        r
    }

    fn as_bytes(&self) -> Vec<u8> {
        self.data()
    }
}

impl IFZS {
    pub fn new(state: &FrameStack, instruction: &Instruction) -> IFZS {
        let ifhd = IFhd::new_v1(state.get_memory(), instruction.get_branch_offset());
        let cmem = CMem::new(&state.get_memory());
        let stks = Stks::from(state);

        IFZS { ifhd, cmem: Some(cmem), umem: None, stks }
    }
}

impl TryFrom<&Vec<u8>> for IFZS {
    type Error = InfocomError;

    fn try_from(data: &Vec<u8>) -> Result<IFZS,Self::Error> {
        let form = bytes_to_string(data, 0, 4);
        if form.as_str() != "FORM" {
            return Err(InfocomError::Memory(format!("Invalid IFF form: '{}'", form)));
        }

        let length = bytes_to_u32(data, 4);

        let sub_id = bytes_to_string(data, 8, 4);
        if sub_id.as_str() != "IFZS" {
            return Err(InfocomError::Memory(format!("Invalid FORM sub-ID: '{}'", sub_id)));
        }

        let mut ifhd:Option<IFhd> = None;
        let mut cmem:Option<CMem> = None;
        let mut umem:Option<UMem> = None;
        let mut stks:Option<Stks> = None;

        let mut offset:usize = 12;

        // Data buffer includes 8 additional bytes (FORM and length)
        let limit:usize = length as usize  + 8;

        while offset < limit {
            let id = bytes_to_string(data, offset, 4);
            let length = bytes_to_u32(data, offset + 4);
            debug!("Chunk {} length ${:06x}", id, length);
            let d = data[offset+8..offset+8+length as usize].to_vec();
            match id.as_str() {
                "IFhd" => ifhd = Some(IFhd::from(&d)),
                "UMem" => umem = Some(UMem::from(&d)),
                "CMem" => cmem = Some(CMem::from(&d)),
                "Stks" => stks = Some(Stks::from(&d)),
                _ => debug!("Ignored chunk ID: {}", id)
            }

            // Skip ID (4 bytes) and length (4 bytes) and data (length bytes)
            offset += 8 + length as usize;
            // Skip a padding byte if the resulting offset is odd
            if offset.rem_euclid(2) == 1 {
                offset += 1;
            }
        }
        
        Ok(IFZS { ifhd: ifhd.unwrap(), cmem, umem, stks: stks.unwrap() })
    }
}




