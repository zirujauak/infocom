pub mod quetzal;
pub mod blorb;

fn u32_to_bytes(v: u32) -> Vec<u8> {
    let mut r = Vec::new();
    r.push(((v >> 24) & 0xFF) as u8);
    r.push(((v >> 16) & 0xFF) as u8);
    r.push(((v >> 8) & 0xFF) as u8);
    r.push((v & 0xFF) as u8);

    r
}

fn u16_to_bytes(v: u16) -> Vec<u8> {
    let mut r = Vec::new();
    r.push(((v >> 8) & 0xFF) as u8);
    r.push((v & 0xFF) as u8);

    r
}

fn bytes_to_u32(buf: &Vec<u8>, offset: usize) -> u32 {
    let mut r:u32 = 0;

    r |= ((buf[offset] as u32) << 24) & 0xFF000000;
    r |= ((buf[offset+1] as u32) << 16) & 0xFF0000;
    r |= ((buf[offset+2] as u32) << 8) & 0xFF00;
    r |= (buf[offset+3] as u32) & 0xFF;

    r
}

fn bytes_to_u16(buf: &Vec<u8>, offset: usize) -> u16 {
    let mut r:u16 = 0;

    r |= ((buf[offset] as u16) << 8) & 0xFF00;
    r |= (buf[offset+1] as u16) & 0xFF;

    r
}

fn bytes_to_string(buf: &Vec<u8>, offset: usize, length: usize) -> String {
    let mut r = String::new();
    for i in 0..length {
        r.push(buf[offset + i] as char);
    }

    r
}

pub trait Chunk {
    fn id(&self) -> String;
    fn length(&self) -> u32;
    fn data(&self) -> Vec<u8>;

    fn as_bytes(&self) -> Vec<u8> {
        let mut d = Vec::new();

        // ID: 4 bytes
        for c in self.id().chars() {
            d.push(c as u8);
        }

        // Length: 4 bytes
        d.append(&mut u32_to_bytes(self.length()));

        // Data: length bytes
        d.append(&mut self.data());

        // If length is odd, add a padding byte
        if self.length().rem_euclid(2) == 1 {
            d.push(0);
        }

        d
    }
}
