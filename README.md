# Infocom

A Rust implementation of an infocom interpreter according to the [Z-Machine specification 1.1](https://www.inform-fiction.org/zmachine/standards/z1point1).

Implementation of instructions is on-going.  Currently most of the version 3 Z-Machine is supported.  Input and output streams aren't done yet, and full sound effect support (for Lurking Horror) is also TBD.

This project targets full cross-platform V3, V4, V5, V7 and V8 compatibility.  Support for the graphics-enabled V6 instruction set is likely to be platform-specific and I haven't researched Rust bindings for various OS windows systems.

Support for the [Quetzal](http://inform-fiction.org/zmachine/standards/quetzal/index.html) save file format has been implemented.
Support for the [Blorb](http://www.eblong.com/zarf/blorb/blorb.html) resource file format planned.

[Easycurses](https://docs.rs/crate/easycurses/0.13.0) is used to provide a cross-platform interface.  Creation of more advanced platform specific interfaces is still a possibility.

#### Usage

```
$ cargo run -- <story-file>
```

This _should_ open a curses window and begin executing the Z-Machine code.

#### Supported
* Most of the V3 instruction set - can play Seastalker through to autopiloting to the research dome
* V3 screen model, including window splitting (Seastalker uses this for automated sonar display)
* V3 Quetzal save/restore

#### Roadmap
* Finish V3 instruction set implementation - input streams, output stream 4, full sound effects (Lurking Horror)
* Add "intelligent" window resize support
* Read story file executables and/or resources from Blorb files.
* Implement V4 instruction set variants and new instructions
* Implement V5 instruction set variants and new instructions (which should also support V7 and V8)
